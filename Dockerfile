#https://nodejs.org/fr/docs/guides/nodejs-docker-webapp/
FROM node:alpine3.11

# Create an application directory
RUN mkdir -p /app

# The /app directory should act as the main application directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 8000

# Start the app
CMD [ "npm", "start" ]

# docker build -t hackathon-frontend-image:latest .
#
# docker run -d -p 80:8000 hackathon-frontend-image:latest
#