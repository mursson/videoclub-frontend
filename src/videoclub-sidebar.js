import { html, LitElement } from "lit";

class VideoclubSidebar extends LitElement {

  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <aside>
            <section>
                <div class="mt-5">
                    <button
                      class="btn btn-success w-100"
                      @click="${this.newMovie}">
                      <strong style="font-size: 20px">Añadir película</strong>
                    </button>
                    <button
                      class="btn btn-success w-100"
                      @click="${this.newClient}">
                      <strong style="font-size: 20px">Añadir cliente</strong>
                    </button>
                </div>
            </section>
        </aside>
        `;
  }

  newMovie(e){
    console.log("newMovie en videoclub-sidebar");
    this.dispatchEvent(new CustomEvent("new-movie", {}));
  }

  newClient(e){
    console.log("newClient en videoclub-sidebar");
    this.dispatchEvent(new CustomEvent("new-client", {}));
  }
}

customElements.define("videoclub-sidebar", VideoclubSidebar);
