import { html, LitElement } from "lit";

class VideoclubFilmDetail extends LitElement {

  static get properties() {
    return {
      movie : {type: Object}
    };
  }

  constructor() {
    super();
    this.movie = {};
  }

  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <div class="card">
      <img src="src/img/${this.movie.photo}" height="380" width="273" alt="${this.movie.id}">
      <div class="card-body">
        <h5 class="card-title">${this.movie.desc}</h5>
        <p class="card-text">${this.movie.genre}</p>
        <p class="card-text">${this.movie.price} €</p>
        <p class="card-text">${this.movie.quantity} unidades</p>
      </div>
      <div class="card-footer">
        <button class="btn btn-danger col-5 offset-1" @click="${this.deleteMovie}">Borrar</button>
        <button class="btn btn-primary col-5" @click="${this.rentMovie}">Alquilar</button>
      </div>
    </div>
    `
  }

  rentMovie(e) {
    console.log("rentMovie en videoclub-film-detail");
    this.dispatchEvent(
      new CustomEvent(
        "rent-movie",
        {
          detail: {
            id: this.movie.id
          }
        }
      )
    )
  }

  deleteMovie(e) {
    console.log("deleteMovie en videoclub-film-detail");
    this.dispatchEvent(
      new CustomEvent(
        "delete-movie",
        {
          detail: {
            id: this.movie.id
          }
        }
      )
    )
  }

}

customElements.define("videoclub-film-detail", VideoclubFilmDetail);
