import { html, LitElement } from "lit";

class VideoclubMovieForm extends LitElement {

  static get properties() {
    return {
      movie: {type: Object},
      editingMovie: {type: Boolean}
    };
  }

  constructor() {
    super();
    this.resetFormData();
  }

  render(){
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <div>
        <form>
            <div class="form-group">
                <label>Identificador</label>
                <input
                    type="text"
                    class="form-control"
                    .value="${this.movie.id}"
                    placeholder="Identificador"
                    @input="${this.updateId}"
                    ?disabled="${this.editingMovie}"
                />
            </div>
            <div class="form-group">
                <label>Nombre completo</label>
                <input
                    type="text"
                    class="form-control"
                    .value="${this.movie.desc}"
                    placeholder="Nombre película"
                    @input="${this.updateDesc}"
                    ?disabled="${this.editingMovie}"
                />
            </div>
            <div class="form-group">
                <label>Precio</label>
                <input
                    type="text"
                    class="form-control"
                    .value="${this.movie.price}"
                    placeholder="Precio"
                    @input="${this.updatePrice}">
                </textarea>
            </div>
            <div class="form-group">
                <label>Género</label>
                <input
                    type="text"
                    class="form-control"
                    .value="${this.movie.genre}"
                    placeholder="Género"
                    @input="${this.updateGenre}"
                />
            </div>
            <div class="form-group">
            <label>Foto</label>
            <input
                type="text"
                class="form-control"
                .value="${this.movie.photo}"
                placeholder="Foto"
                @input="${this.updatePhoto}"
            />
        </div>
            <button class="btn btn-primary" @click="${this.goBack}"><strong>Cancelar</strong></button>
            <button class="btn btn-success" @click="${this.storeMovie}"><strong>Guardar</strong></button>
        </form>
    </div>
    `;
  }

  goBack(e){
    console.log("goBack");
    console.log("Se ha pulsado botón atrás formulario movie-form");
    e.preventDefault();
    this.dispatchEvent(new CustomEvent("movie-form-close", {}));
    this.resetFormData();
}

storeMovie(e){
    console.log('storeMovie');
    console.log('Se va guardar');
    console.log(this.movie);
    e.preventDefault();

    this.dispatchEvent(
        new CustomEvent(
            "movie-form-store",
            {
                detail: {
                    movie: {
                        id: this.movie.id,
                        desc: this.movie.desc,
                        price: this.movie.price,
                        genre: this.movie.genre,
                        photo: this.movie.photo
                    },
                    editingMovie: this.editingMovie
                }
            }
        )
    );
    this.resetFormData();
}

updateDesc(e){
    console.log("updateDesc");
    console.log("Actualizando la propiedad desc con el valor " + e.target.value);
    this.movie.desc = e.target.value;
}

updatePrice(e){
    console.log("updatePrice");
    console.log("Actualizando la propiedad price con el valor " + e.target.value);
    this.movie.price = e.target.value;
}

updateGenre(e){
    console.log("updateGenre");
    console.log("Actualizando la propiedad genre con el valor " + e.target.value);
    this.movie.genre = e.target.value;
}

updatePhoto(e){
    console.log("updateGenre");
    console.log("Actualizando la propiedad photo con el valor " + e.target.value);
    this.movie.photo = e.target.value;
}

updateId(e){
    console.log("updatId");
    console.log("Actualizando la propiedad id con el valor " + e.target.value);
    this.movie.id = e.target.value;
}

  resetFormData(){
    console.log("resetFormData");
    this.movie = {};
    this.movie.id = "";
    this.movie.desc = "";
    this.movie.price = "";
    this.movie.genre = "";
    this.movie.photo = "";
    this.editingMovie = false;
  }
}

customElements.define("videoclub-movie-form", VideoclubMovieForm);
