import { LitElement } from "lit";

class VideoclubApi extends LitElement {

  static get properties() {
    return {
      movies: { type: Array },
      clients: { type: Array },
      deletedMovie: { type: String},
      deletedClient: { type: String},
      addedMovieOk: {type: Boolean},
      addedClientOk: {type: Boolean},
      addedRentOk: {type: Boolean}
    };
  }

  constructor() {
    super();
    this.deletedMovie = "";
    this.deletedClient = "";
    this.addedMovieOk = false;
    this.addedClientOk = false;
    this.addedRentOk = false;
    this.movies = [
      // {
      //   "id": "1",
      //   "desc": "Titanic",
      //   "price": 5.0,
      //   "quantity": "2",
      //   "genre": "Melodrama",
      //   "photo": "1.jpg"
      // },
      // {
      //   "id": "2",
      //   "desc": "Jurassic Park",
      //   "price": 5.0,
      //   "quantity": "2",
      //   "genre": "Thriller",
      //   "photo": "2.jpg"
      // },
      // {
      //   "id": "3",
      //   "desc": "Pulp Fiction",
      //   "price": 5.0,
      //   "quantity": "2",
      //   "genre": "Thriller violento",
      //   "photo": "3.jpg"
      // },
      // {
      //   "id": "4",
      //   "desc": "Akira",
      //   "price": 5.0,
      //   "genre": "Anime violento",
      //   "photo": "4.jpg"
      // },
      // {
      //   "id": "5",
      //   "desc": "Aladdin",
      //   "price": 5.0,
      //   "genre": "Dibujos animados",
      //   "photo": "5.jpg"
      // },
    ];

    this.clients = [
      // {
      //     "id": "1",
      //     "name": "Antonio",
      //     "age": 40
      // },
      // {
      //     "id": "2",
      //     "name": "Lin",
      //     "age": 40
      // }
    ];

    this.getMovies();
    this.getClients();
  }

  getMovies() {
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200) {
        let APIresponse = JSON.parse(xhr.responseText);
        console.log(APIresponse);
        this.movies = APIresponse;
      }
    };

    xhr.open("GET", "http://localhost:8081/hackaton/films");
    xhr.send();
  }

  getClients() {
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200) {
        let APIresponse = JSON.parse(xhr.responseText);
        console.log(APIresponse);
        this.clients = APIresponse;
      }
    };

    xhr.open("GET", "http://localhost:8081/hackaton/clients");
    xhr.send();
  }

  deleteClient(id) {
    console.log("deleteClient en videoclub-api");
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200 || xhr.status === 204) {
        let APIresponse = JSON.parse(xhr.responseText);
        console.log(APIresponse);
        this.deletedClient = id;
      } else {
        this.deletedClient = null;
      }
    };

    xhr.open("DELETE", `http://localhost:8081/hackaton/clients/${id}`);
    xhr.send();
  }

  deleteMovie(id) {
    console.log("deleteMovie en videoclub-api");
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200 || xhr.status === 204) {
        //let APIresponse = JSON.parse(xhr.responseText);
        //console.log(APIresponse);
        this.deletedMovie = id;
      } else {
        this.deletedMovie = null;
      }
    };

    xhr.open("DELETE", `http://localhost:8081/hackaton/films/${id}`);
    xhr.send();
  }

  addMovie(movie) {
    console.log("addMovie en videoclub-api");
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200 || xhr.status === 201) { //201 Created (tipica respuesta ante una peticion PUT o POST)
        let APIresponse = JSON.parse(xhr.responseText);
        console.log(APIresponse);
        this.addedMovieOk = true;
      }
    };    
    xhr.open("POST", "http://localhost:8081/hackaton/films/");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(movie));
  }

  addClient(client) {
    console.log("addClient en videoclub-api");    
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200 || xhr.status === 201) { //201 Created (tipica respuesta ante una peticion PUT o POST)
        let APIresponse = JSON.parse(xhr.responseText);
        console.log(APIresponse);
        this.addedClientOk = true;
      }
    };   
    xhr.open("POST", "http://localhost:8081/hackaton/clients/");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(client));
  }

  rentMovie(rent) {
    console.log("rentMovie en videoclub-api");    
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200 || xhr.status === 201) { //201 Created (tipica respuesta ante una peticion PUT o POST)
        let APIresponse = JSON.parse(xhr.responseText);
        console.log(APIresponse);
        this.addedRentOk = true;
      } else {
        this.addedRentOk = false;
      }
    };   
    xhr.open("POST", "http://localhost:8081/hackaton/rents/");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify(rent));
  }

  updated(changeProperties) {
    console.log("updated en videoclub-api");
    if (changeProperties.has("movies")) {
      this.dispatchEvent(
        new CustomEvent(
          "get-movies-ok",
          {
            detail: {
              movies: this.movies
            }
          }
        )
      )
    }

    if (changeProperties.has("deletedMovie")) {
      console.log("Se va a lanzar evento de borrar con id " + this.deletedMovie);
      this.dispatchEvent(
        new CustomEvent(
          "delete-movie",
          {
            detail: {
              id: this.deletedMovie
            }
          }
        )
      )
    }

    if (changeProperties.has("clients")) {
      console.log("Se va a lanzar evento de get-clients");
      console.log(this.clients);
      this.dispatchEvent(
        new CustomEvent(
          "get-clients",
          {
            detail: {
              clients: this.clients
            }
          }
        )
      )
    }

    if (changeProperties.has("addedMovieOk")) {
      console.log("Se va a lanzar evento de add movie ok");
      this.dispatchEvent(
        new CustomEvent(
          "added-movie-ok",
          {
            detail: {
              addedMovie: this.addedMovieOk
            }
          }
        )
      )
      this.addedMovieOk = false;
    }

    if (changeProperties.has("addedClientOk")) {
      console.log("Se va a lanzar evento de add client ok");
      this.dispatchEvent(
        new CustomEvent(
          "added-client-ok",
          {
            detail: {
              addedClient: this.addedClientOk
            }
          }
        )
      )
      this.addedClientOk = false;
    }

    if (changeProperties.has("addedRentOk")) {
      console.log("Se va a lanzar evento de add rent ok");
      this.dispatchEvent(
        new CustomEvent(
          "added-rent-ok",
          {
            detail: {
              addedRentOk: this.addedRentOk
            }
          }
        )
      )
      this.addedRentOk = false;
    }
  }

  
}

customElements.define("videoclub-api", VideoclubApi);
