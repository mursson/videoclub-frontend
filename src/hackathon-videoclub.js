import { html, LitElement } from "lit";
import '../src/videoclub-header.js';
import '../src/videoclub-sidebar.js';
import '../src/videoclub-main.js';
class HackathonVideoclub extends LitElement {

  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <videoclub-header></videoclub-header>
    <h2 class="text-center">Peliculas</h2>
    <div class="row">
      <videoclub-sidebar @new-movie="${this.newMovie}" @new-client="${this.newClient}" class="col-2"></videoclub-sidebar>
      <videoclub-main class="col-10"></videoclub-main>
    </div>
    `
  }

  newMovie(e){
    console.log("newMovie en hackathon-videoclub");
    this.shadowRoot.querySelector("videoclub-main").showAddClientForm = false;
    this.shadowRoot.querySelector("videoclub-main").showMovieForm = true;
  }

  newClient(e){
    console.log("newClient en hackathon-videoclub");
    this.shadowRoot.querySelector("videoclub-main").showMovieForm = false;
    this.shadowRoot.querySelector("videoclub-main").showAddClientForm = true;
  }
}

customElements.define("hackathon-videoclub", HackathonVideoclub);
