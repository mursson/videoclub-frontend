import { html, LitElement } from "lit";
import '../src/videoclub-api.js';
import '../src/videoclub-film-detail.js';
import '../src/videoclub-client.js';
import '../src/videoclub-movie-form.js';
import '../src/videoclub-client-form.js';

class VideoclubMain extends LitElement {

  static get properties() {
    return {
      movies: { type: Array },
      clients: { type: Array },
      showMovieForm: {type: Boolean},
      showAddClientForm: {type: Boolean},
      rentData: {type: Object},
      moviesAux: {type: Array}
    };
  }

  constructor() {
    super();
    this.movies = [];
    this.clients = [];
    this.rentData = {};
    this.showMovieForm = false;
    this.showAddClientForm = false;
  }

  render() {
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <div class="row" id="movieList">
        <main class="row row-cols-1 row-cols-sm-4">
          ${this.movies.map(
            movie => html`<videoclub-film-detail
                    .movie="${movie}"
                    @rent-movie="${this.showClients}"
                    @delete-movie="${this.deleteMovie}"
                      ></videoclub-film-detail>`
      )}
        </main>
      </div>

      <div class="row d-none" id="oneMovieList">
      <main class="row row-cols-1 row-cols-sm-4">
        ${this.movies.map(
          movie => html`<videoclub-film-detail
                  .movie="${movie}"
                  @rent-movie="${this.showClients}"
                  @delete-movie="${this.deleteMovie}"
                    ></videoclub-film-detail>`
       )}
      </main>
      </div>

      <div class="row d-none" id="clientList">
        <div class="container">
        <br /><br />
          <div class="row">
            <div class="col d-flex justify-content-center">
              <h5 class="card-title"><b>Nombre</b></h5>
            </div>
            <div class="col">
              <h5 class="card-title"><b>Edad</b></h5>
            </div>
            <div class="col">
              <span></span>
            </div>
          </div>
          <br /><br />
        </div>
      ${this.clients.map(
        client => html`<videoclub-client
                        .client="${client}"
                        @client-selected="${this.clientSelected}">
                        </videoclub-client>`
    )}
        <div class="row row-cols-3 row-cols-md-5 d-flex justify-content-end">
            <button class="btn btn-warning col-5" @click="${this.cancel}"><strong>Volver</strong></button>
            <button class="btn btn-success col-5 offset-1" @click="${this.confirmPurchase}">Confirmar compra</button>
        </div>
      </div>
      <div class="row d-none" id="movieForm">
        <videoclub-movie-form
          @movie-form-close="${this.movieFormClose}"
          @movie-form-store="${this.movieFormStore}">
        </videoclub-movie-form>
      </div>
      <div class="row d-none" id="clientForm">
        <videoclub-client-form
          @client-form-close="${this.clientFormClose}"
          @client-form-store="${this.clientFormStore}">
        </videoclub-client-form>
      </div>
      <videoclub-api
        @get-clients="${this.updatedClients}"
        @get-movies-ok="${this.updatedMovies}"
        @delete-movie="${this.isMovieDeleted}"
        @added-movie-ok="${this.addedMovie}"
        @added-client-ok="${this.addedClient}"
        @added-rent-ok="${this.addedRent}"
        id="videoclub-api">
        </videoclub-api>
      </div>
   `
  }

  updated(changedProperties){
    console.log("updated en videoclub-main");

    if (changedProperties.has("showMovieForm")){
        console.log("Ha cambiado el valor de la propiedad showMovieForm en videoclub-main");
        if (this.showMovieForm){
          this.showVisualMovieForm();
        } else {
          this.hideVisualMovieForm();
        }
    }

    if (changedProperties.has("showAddClientForm")){
      console.log("Ha cambiado el valor de la propiedad showAddClientForm en videoclub-main");
      if (this.showAddClientForm){
        this.showVisualAddClientForm();
      } else {
        this.hideVisualAddClientForm();
      }
    }
  }

  movieFormClose(){
    this.showMovieForm = false;
  }

  clientFormClose(){
    this.showAddClientForm = false;
  }

  movieFormStore(e){
    let movieToSave = e.detail.movie;
    this.shadowRoot.querySelector("videoclub-api").addMovie(movieToSave);
    this.hideVisualMovieForm();
  }

  clientFormStore(e){
    let clientToSave = e.detail.client;
    this.shadowRoot.querySelector("videoclub-api").addClient(clientToSave);
    this.hideVisualAddClientForm();
  }

  showVisualMovieForm(){
    this.shadowRoot.getElementById("movieList").classList.add("d-none");
    this.shadowRoot.getElementById("clientList").classList.add("d-none");
    this.shadowRoot.getElementById("movieForm").classList.remove("d-none");
    this.shadowRoot.getElementById("clientForm").classList.add("d-none");
  }

  hideVisualMovieForm(){
    this.shadowRoot.getElementById("movieList").classList.remove("d-none");
    this.shadowRoot.getElementById("movieForm").classList.add("d-none");
    //this.shadowRoot.getElementById("clientForm").classList.add("d-none");
    this.showMovieForm = false;
    this.showAddClientForm = false;
  }

  showVisualAddClientForm(){
    this.shadowRoot.getElementById("movieList").classList.add("d-none");
    this.shadowRoot.getElementById("clientList").classList.add("d-none");
    this.shadowRoot.getElementById("movieForm").classList.add("d-none");
    this.shadowRoot.getElementById("clientForm").classList.remove("d-none");
  }

  hideVisualAddClientForm(){
    this.shadowRoot.getElementById("movieList").classList.remove("d-none");
    this.shadowRoot.getElementById("clientForm").classList.add("d-none");
    this.showMovieForm = false;
    this.showAddClientForm = false;
  }

  showClients(e) {
    console.log("showClients en videoclub-main");
    this.rentData.movieId = e.detail.id;
    this.moviesAux = this.movies;
    this.movies = this.movies.filter(
      movie => movie.id === e.detail.id);
    this.shadowRoot.getElementById("clientList").classList.remove("d-none");
    this.shadowRoot.getElementById("movieList").classList.add("d-none");
    this.shadowRoot.getElementById("oneMovieList").classList.remove("d-none");
  }

  clientSelected(e){
    console.log("clientSelected en videoclub-main");
    this.rentData.clientId = e.detail.client.id;
  }

  confirmPurchase(e){
    if (this.rentData.clientId && this.rentData.movieId){
      console.log("confirmPurchase en videoclub-main");
      let rentObj = {};
      rentObj.id = String(this.getRandomArbitrary(1, 10000));
      rentObj.userId = this.rentData.clientId;
      rentObj.filmId = this.rentData.movieId;
      this.shadowRoot.querySelector("videoclub-api").rentMovie(rentObj);
      this.rentData = {};
      this.movies = this.moviesAux;
      this.shadowRoot.getElementById("clientList").classList.add("d-none");
    } else {
      alert("Por favor seleccione un cliente");
    }
  }

  addedRent(e){
     if (e.detail.addedRentOk){
       alert("Operación realizada correctamente");
     }
  }

  // Retorna un número aleatorio entre min (incluido) y max (excluido)
  getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * max) + min
  }


  //Envia peticion HTTP de delete
  deleteMovie(e) {
    console.log("deleteMovie en videoclub-main");

    //Lanzar delete del videoclub-api
    this.shadowRoot.querySelector("videoclub-api").deleteMovie(e.detail.id);
  }

  //Recibe respuesta de borrado de pelicula
  isMovieDeleted(e) {
    console.log("isMovieDeleted en videoclub-main");
    //Si se devolvió un 200 de borrar pelicula, tendremos el id
    if (e.detail.id !== null && e.detail.id !== undefined) {
      this.movies = this.movies.filter(
        movie => movie.id != e.detail.id
      );
    } else {
      alert("No se pudo borrar la película porque no se recibió un 200/204 del back. Consultar pestaña Network de la devtool");
    }
  }

  cancel(e) {
    this.rentData = {};
    this.movies = this.moviesAux;
    this.shadowRoot.getElementById("clientList").classList.add("d-none");
    this.shadowRoot.getElementById("movieList").classList.remove("d-none");
    this.shadowRoot.getElementById("oneMovieList").classList.add("d-none");
  }

  updatedMovies(e) {
    this.movies = e.detail.movies;
  }

  updatedClients(e) {
    console.log("updatedClients en videoclub-main-js");
    console.log(e);
    this.clients = e.detail.clients;
  }

  addedMovie(e){
    this.shadowRoot.querySelector("videoclub-api").getMovies();
  }

  addedClient(e){
    this.shadowRoot.querySelector("videoclub-api").getClients();
  }

}


customElements.define("videoclub-main", VideoclubMain);
