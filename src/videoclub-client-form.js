import { html, LitElement } from "lit";

class VideoclubClientForm extends LitElement {

  static get properties() {
    return {
      client: {type: Object},
      editingClient: {type: Boolean}
    };
  }

  constructor() {
    super();
    this.resetFormData();
  }

  render(){
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <div>
        <form>
            <div class="form-group">
                <label>Nombre completo</label>
                <input
                    type="text"
                    class="form-control"
                    .value="${this.client.name}"
                    placeholder="Nombre completo"
                    @input="${this.updateName}"
                    ?disabled="${this.editingClient}"
                />
            </div>
            <div class="form-group">
                <label>Edad</label>
                <input
                    type="text"
                    class="form-control"
                    .value="${this.client.age}"
                    placeholder="Edad"
                    @input="${this.updateAge}"
                    ?disabled="${this.editingClient}">
                </textarea>
            </div>
            <button class="btn btn-primary" @click="${this.goBack}"><strong>Cancelar</strong></button>
            <button class="btn btn-success" @click="${this.storeClient}"><strong>Guardar</strong></button>
        </form>
    </div>
    `;
  }

  goBack(e){
    console.log("goBack en videoclub-client-form");
    e.preventDefault();
    this.dispatchEvent(new CustomEvent("client-form-close", {}));
    this.resetFormData();
  }

  storeClient(e){
      console.log('storeClient en videoclub-client-form');
      console.log(this.client);
      e.preventDefault();

      this.dispatchEvent(
          new CustomEvent(
              "client-form-store",
              {
                  detail: {
                      client: {
                          name: this.client.name,
                          age: this.client.age
                      },
                      editingClient: this.editingClient
                  }
              }
          )
      );
      this.resetFormData();
  }

  updateName(e){
      console.log("updateName en videoclub-client-form");
      console.log("Actualizando la propiedad name con el valor " + e.target.value);
      this.client.name = e.target.value;
  }

  updateAge(e){
      console.log("updateAge en videoclub-client-form");
      console.log("Actualizando la propiedad age con el valor " + e.target.value);
      this.client.age = e.target.value;
  }

  resetFormData(){
    console.log("resetFormData en videoclub-client-form");
    this.client = {};
    this.client.age = "";
    this.client.name = "";
    this.editingClient = false;
  }
}

customElements.define("videoclub-client-form", VideoclubClientForm);
