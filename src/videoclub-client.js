import { html, LitElement } from "lit";

class VideoclubClient extends LitElement {

  static get properties() {
    return {
      client: {type: Object}
    };
  }

  constructor() {
    super();
    this.client = {};
  }
  
  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <div class="container">
      <div class="row row-cols-1 row-cols-sm-4">
        <div class="col">      
          <h5 class="card-title">${this.client.name}</h5>
        </div>      
        <div class="col">      
        <p class="card-text">${this.client.age}</p>
        </div>      
          <div class="col">   
        <button class="btn btn-outline-secondary col-6 offset-1" @click="${this.selectClientToRent}">Seleccionar</button>
        </div>
      </div>  
      <br />  
    </div>
    <br /><br /><br />
    `
  }

  selectClientToRent(e){
    this.dispatchEvent(
      new CustomEvent(
          "client-selected",
          {
              detail: {
                  client: {
                      id: this.client.id
                  }
              }
          }
      )
  );
  }
}

customElements.define("videoclub-client", VideoclubClient);